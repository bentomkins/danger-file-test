import { danger, message, warn, fail } from "danger";
import YAML from 'yaml';
import fs from 'fs';


const VALIDATION_FILE = YAML.parse(fs.readFileSync('.valid', 'utf8'));
console.log(JSON.stringify(VALIDATION_FILE, null, 2));
const LICENSE_FILE = fs.readFileSync('license', 'utf8');


const findMatches = (file: string, regex: RegExp) => (
  file.split(/[\r\n]/)
    .map((line, index) => {
      if (regex.test(line))
        return { line, number: ++index, match: line.match(regex)[0] }
    }).filter(Boolean)
);

const regex = /software[^,]+/i;
console.log(findMatches(LICENSE_FILE, regex));


// Add a CHANGELOG entry for app changes
const hasChangelog = danger.git.modified_files.includes("changelog.md");

if (!hasChangelog) {
  fail("Please add a changelog entry for your changes.");
}
